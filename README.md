## Carvan Insurance Challenge

This [data set](https://www.kaggle.com/uciml/caravan-insurance-challenge) used in the CoIL 2000 Challenge contains information on customers of an insurance company. The data consists of 86 variables and includes product usage data and socio-demographic data derived from zip area codes. The data was collected to answer the following question: Can you predict who would be interested in buying a caravan insurance policy and give an explanation why?

#### Setup

Clone this repository by following command:

```
git clone https://gitlab.com/ankon111/carvan_insurance_challenge.git
```

#### Python Environment
This project is implemented in python. We recommend to use anaconda(miniconda) environment for Windows and Linux platforms.
To create new environment call:

```
conda env create --name envname --file=environment.yml
```

#### Run

Open a terminal and browse to the project directory. If you create the environment from the file above then your environment_name is `carvan` otherwise put the environment name you created.
Run the following command:

```
> source activate <environment_name>
> jupyter notebook

```

Then open the notebook and run each cell to see the output.

